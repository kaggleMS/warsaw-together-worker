# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('starttime', models.DateTimeField()),
                ('endtime', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('starttime', models.DateTimeField()),
                ('endtime', models.DateTimeField()),
                ('geog', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('address', models.TextField()),
                ('nazwa', models.CharField(max_length=180)),
                ('repeat_type', models.CharField(max_length=180)),
                ('max_participants', models.IntegerField(null=True)),
                ('req_min_participants', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('events', models.ManyToManyField(to='world.Event', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('username', models.CharField(max_length=180)),
                ('username_canonical', models.CharField(unique=True, max_length=180)),
                ('email', models.CharField(max_length=180)),
                ('email_canonical', models.CharField(unique=True, max_length=180)),
                ('geog', django.contrib.gis.db.models.fields.PointField(srid=4326, null=True)),
                ('bio', models.TextField(null=True)),
                ('enabled', models.BooleanField()),
                ('salt', models.CharField(max_length=255, null=True, blank=True)),
                ('password', models.CharField(max_length=255)),
                ('last_login', models.DateTimeField(null=True, blank=True)),
                ('confirmation_token', models.CharField(max_length=180, unique=True, null=True, blank=True)),
                ('password_requested_at', models.DateTimeField(null=True, blank=True)),
                ('roles', models.TextField()),
                ('interests', models.ManyToManyField(to='world.Interest', null=True, blank=True)),
                ('participatedin', models.ManyToManyField(to='world.Event', null=True, blank=True)),
            ],
            options={
                'db_table': 'users',
            },
        ),
        migrations.CreateModel(
            name='WorldBorder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('area', models.IntegerField()),
                ('pop2005', models.IntegerField(verbose_name='Population 2005')),
                ('fips', models.CharField(max_length=2, verbose_name='FIPS Code')),
                ('iso2', models.CharField(max_length=2, verbose_name='2 Digit ISO')),
                ('iso3', models.CharField(max_length=3, verbose_name='3 Digit ISO')),
                ('un', models.IntegerField(verbose_name='United Nations Code')),
                ('region', models.IntegerField(verbose_name='Region Code')),
                ('subregion', models.IntegerField(verbose_name='Sub-Region Code')),
                ('lon', models.FloatField()),
                ('lat', models.FloatField()),
                ('mpoly', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326)),
            ],
        ),
        migrations.AddField(
            model_name='availability',
            name='interest',
            field=models.ForeignKey(to='world.Interest'),
        ),
        migrations.AddField(
            model_name='availability',
            name='user',
            field=models.ForeignKey(to='world.User'),
        ),
    ]

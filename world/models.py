# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance  




class WorldBorder(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = models.CharField(max_length=50)
    area = models.IntegerField()
    pop2005 = models.IntegerField('Population 2005')
    fips = models.CharField('FIPS Code', max_length=2)
    iso2 = models.CharField('2 Digit ISO', max_length=2)
    iso3 = models.CharField('3 Digit ISO', max_length=3)
    un = models.IntegerField('United Nations Code')
    region = models.IntegerField('Region Code')
    subregion = models.IntegerField('Sub-Region Code')
    lon = models.FloatField()
    lat = models.FloatField()

    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField()

    # Returns the string representation of the model.
    def __str__(self):              # __unicode__ on Python 2
        return self.name
    

    
    
class Event(models.Model):
    name = models.CharField(max_length=30)
    starttime = models.DateTimeField()
    endtime = models.DateTimeField()
    geog = models.PointField()
    
    address = models.TextField()
    nazwa = models.CharField(max_length=180)
    repeat_type = models.CharField(max_length=180)
    max_participants = models.IntegerField(null=True)
    req_min_participants = models.IntegerField(null=True)
    def __unicode__(self):
       return 'Policy: ' + self.name

    def znajdz_uczestnikow(self, radius=10):
        avs = Availability.objects.filter(
            #interest=self.interest,
            #Availability.objects.filter(starttime__lte=ev.endtime,endtime__gte=ev.starttime)
            starttime__lte=self.endtime,
            endtime__gte=self.starttime,
            user__geog__distance_lt=(
            self.geog, Distance(km=radius))).all()
        for av in avs:
            av.user.participatedin.add(self)
            av.user.save()
        return  [av.user for av in avs]
    

class Interest(models.Model):
    name = models.CharField(max_length=30)
    events = models.ManyToManyField(Event,null=True, blank=True)
    def __unicode__(self):
       return 'Policy: ' + self.name

class User(models.Model):
    id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=180)
    username_canonical = models.CharField(unique=True, max_length=180)
    email = models.CharField(max_length=180)
    email_canonical = models.CharField(unique=True, max_length=180)
    geog = models.PointField(null=True)
    bio = models.TextField(null=True)
    enabled = models.BooleanField()
    salt = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255)
    last_login = models.DateTimeField(blank=True, null=True)
    confirmation_token = models.CharField(unique=True, max_length=180, blank=True, null=True)
    password_requested_at = models.DateTimeField(blank=True, null=True)
    roles = models.TextField()
    interests = models.ManyToManyField(Interest, blank=True, null=True)
    participatedin = models.ManyToManyField(Event, blank=True, null=True)
    def __unicode__(self):
       return 'Policy: ' + self.username
    class Meta:
        #managed = False
        db_table = 'users'
'''
class User(models.Model):
    #id = Column(Integer, primary_key=True)
    username = models.CharField()
    username_canonical = models.CharField()
    email = model.CharField()
    email_canonical = model.CharField()
    enabled = models.BooleanField()
    salt = models.TextField()
    password = moddels.CharField()
    last_login = models.DateTimeField()
    confirmation_token = models.TextField()
    password_requested_at = models.DateTimeField()
    roles = models.TextField()
    description = models.TextField()
    birth_year = models.IntegerField()
    surname = model.CharField()
    address = models.TextField()
'''

class Availability(models.Model):
    #id = Column(Integer, primary_key=True)
    #__tablename__ = 'availability'
    user = models.ForeignKey(User)    
    starttime = models.DateTimeField()
    endtime = models.DateTimeField()
    interest = models.ForeignKey(Interest)
    def __unicode__(self):
       return 'Policy: '
    
    def znajdz_kolegow(self):
        return 

'''


from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance  


lat = 52.5
lng = 1.0
radius = 10
point = Point(lng, lat)    
User.objects.filter(geog__distance_lt=(point, Distance(km=radius)))

User.objects.filter(geog__distance_lt=(tomek.geog, Distance(km=radius)))

User.objects.filter(interests=tomek.interest , geog__distance_lt=(tomek.geog, Distance(km=radius)))

'''

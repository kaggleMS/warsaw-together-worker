# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from world.models import User, Event, Interest, Availability
# Register your models here.

admin.site.register(User)
admin.site.register(Event)
admin.site.register(Interest)
admin.site.register(Availability)
#
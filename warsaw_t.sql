--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO geo;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO geo;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO geo;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO geo;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO geo;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO geo;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO geo;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO geo;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO geo;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO geo;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO geo;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO geo;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO geo;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO geo;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO geo;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO geo;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO geo;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO geo;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO geo;

--
-- Name: users; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(180) NOT NULL,
    username_canonical character varying(180) NOT NULL,
    email character varying(180) NOT NULL,
    email_canonical character varying(180) NOT NULL,
    enabled boolean NOT NULL,
    salt character varying(255),
    password character varying(255) NOT NULL,
    last_login timestamp with time zone,
    confirmation_token character varying(180),
    password_requested_at timestamp with time zone,
    roles text NOT NULL,
    bio text,
    geog geometry(Point,4326)
);


ALTER TABLE users OWNER TO geo;

--
-- Name: users_interests; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE users_interests (
    id integer NOT NULL,
    user_id integer NOT NULL,
    interest_id integer NOT NULL
);


ALTER TABLE users_interests OWNER TO geo;

--
-- Name: users_interests_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE users_interests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_interests_id_seq OWNER TO geo;

--
-- Name: users_interests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE users_interests_id_seq OWNED BY users_interests.id;


--
-- Name: users_participatedin; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE users_participatedin (
    id integer NOT NULL,
    user_id integer NOT NULL,
    event_id integer NOT NULL
);


ALTER TABLE users_participatedin OWNER TO geo;

--
-- Name: users_participatedin_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE users_participatedin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_participatedin_id_seq OWNER TO geo;

--
-- Name: users_participatedin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE users_participatedin_id_seq OWNED BY users_participatedin.id;


--
-- Name: world_availability; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE world_availability (
    id integer NOT NULL,
    starttime timestamp with time zone NOT NULL,
    endtime timestamp with time zone NOT NULL,
    interest_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE world_availability OWNER TO geo;

--
-- Name: world_availability_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE world_availability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_availability_id_seq OWNER TO geo;

--
-- Name: world_availability_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE world_availability_id_seq OWNED BY world_availability.id;


--
-- Name: world_event; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE world_event (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    starttime timestamp with time zone NOT NULL,
    endtime timestamp with time zone NOT NULL,
    geog geometry(Point,4326) NOT NULL,
    address text NOT NULL,
    nazwa character varying(180) NOT NULL,
    repeat_type character varying(180) NOT NULL,
    max_participants integer,
    req_min_participants integer
);


ALTER TABLE world_event OWNER TO geo;

--
-- Name: world_event_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE world_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_event_id_seq OWNER TO geo;

--
-- Name: world_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE world_event_id_seq OWNED BY world_event.id;


--
-- Name: world_interest; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE world_interest (
    id integer NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE world_interest OWNER TO geo;

--
-- Name: world_interest_events; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE world_interest_events (
    id integer NOT NULL,
    interest_id integer NOT NULL,
    event_id integer NOT NULL
);


ALTER TABLE world_interest_events OWNER TO geo;

--
-- Name: world_interest_events_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE world_interest_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_interest_events_id_seq OWNER TO geo;

--
-- Name: world_interest_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE world_interest_events_id_seq OWNED BY world_interest_events.id;


--
-- Name: world_interest_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE world_interest_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_interest_id_seq OWNER TO geo;

--
-- Name: world_interest_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE world_interest_id_seq OWNED BY world_interest.id;


--
-- Name: world_worldborder; Type: TABLE; Schema: public; Owner: geo
--

CREATE TABLE world_worldborder (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    area integer NOT NULL,
    pop2005 integer NOT NULL,
    fips character varying(2) NOT NULL,
    iso2 character varying(2) NOT NULL,
    iso3 character varying(3) NOT NULL,
    un integer NOT NULL,
    region integer NOT NULL,
    subregion integer NOT NULL,
    lon double precision NOT NULL,
    lat double precision NOT NULL,
    mpoly geometry(MultiPolygon,4326) NOT NULL
);


ALTER TABLE world_worldborder OWNER TO geo;

--
-- Name: world_worldborder_id_seq; Type: SEQUENCE; Schema: public; Owner: geo
--

CREATE SEQUENCE world_worldborder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE world_worldborder_id_seq OWNER TO geo;

--
-- Name: world_worldborder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: geo
--

ALTER SEQUENCE world_worldborder_id_seq OWNED BY world_worldborder.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_interests ALTER COLUMN id SET DEFAULT nextval('users_interests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_participatedin ALTER COLUMN id SET DEFAULT nextval('users_participatedin_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_availability ALTER COLUMN id SET DEFAULT nextval('world_availability_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_event ALTER COLUMN id SET DEFAULT nextval('world_event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest ALTER COLUMN id SET DEFAULT nextval('world_interest_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest_events ALTER COLUMN id SET DEFAULT nextval('world_interest_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_worldborder ALTER COLUMN id SET DEFAULT nextval('world_worldborder_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add availability	7	add_availability
20	Can change availability	7	change_availability
21	Can delete availability	7	delete_availability
22	Can add world border	8	add_worldborder
23	Can change world border	8	change_worldborder
24	Can delete world border	8	delete_worldborder
25	Can add event	9	add_event
26	Can change event	9	change_event
27	Can delete event	9	delete_event
28	Can add user	10	add_user
29	Can change user	10	change_user
30	Can delete user	10	delete_user
31	Can add interest	11	add_interest
32	Can change interest	11	change_interest
33	Can delete interest	11	delete_interest
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_permission_id_seq', 33, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, false);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	world	availability
8	world	worldborder
9	world	event
10	world	user
11	world	interest
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('django_content_type_id_seq', 11, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2017-10-14 21:28:25.084463+02
2	auth	0001_initial	2017-10-14 21:28:26.109345+02
3	admin	0001_initial	2017-10-14 21:28:26.352938+02
4	admin	0002_logentry_remove_auto_add	2017-10-14 21:28:26.396522+02
5	contenttypes	0002_remove_content_type_name	2017-10-14 21:28:26.462785+02
6	auth	0002_alter_permission_name_max_length	2017-10-14 21:28:26.495581+02
7	auth	0003_alter_user_email_max_length	2017-10-14 21:28:26.529271+02
8	auth	0004_alter_user_username_opts	2017-10-14 21:28:26.561129+02
9	auth	0005_alter_user_last_login_null	2017-10-14 21:28:26.596544+02
10	auth	0006_require_contenttypes_0002	2017-10-14 21:28:26.607603+02
11	auth	0007_alter_validators_add_error_messages	2017-10-14 21:28:26.641891+02
12	auth	0008_alter_user_username_max_length	2017-10-14 21:28:26.742481+02
13	sessions	0001_initial	2017-10-14 21:28:26.974665+02
14	world	0001_initial	2017-10-14 21:28:28.422149+02
15	world	0002_auto_20171014_1939	2017-10-14 21:39:31.106348+02
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('django_migrations_id_seq', 15, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spatial_ref_sys  FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY users (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles, bio, geog) FROM stdin;
1	Michal				t	\N		\N	\N	\N		\N	0101000020E6100000000000000000F03F0000000000404A40
2	Tomek	Tomek2		@	t	\N		\N	\N	\N		\N	0101000020E6100000000000000000F03F0000000000404A40
\.


--
-- Data for Name: users_interests; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY users_interests (id, user_id, interest_id) FROM stdin;
1	2	1
2	1	1
\.


--
-- Name: users_interests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('users_interests_id_seq', 2, true);


--
-- Data for Name: users_participatedin; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY users_participatedin (id, user_id, event_id) FROM stdin;
\.


--
-- Name: users_participatedin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('users_participatedin_id_seq', 1, false);


--
-- Data for Name: world_availability; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY world_availability (id, starttime, endtime, interest_id, user_id) FROM stdin;
\.


--
-- Name: world_availability_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('world_availability_id_seq', 1, false);


--
-- Data for Name: world_event; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY world_event (id, name, starttime, endtime, geog, address, nazwa, repeat_type, max_participants, req_min_participants) FROM stdin;
\.


--
-- Name: world_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('world_event_id_seq', 1, false);


--
-- Data for Name: world_interest; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY world_interest (id, name) FROM stdin;
1	planzowki
\.


--
-- Data for Name: world_interest_events; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY world_interest_events (id, interest_id, event_id) FROM stdin;
\.


--
-- Name: world_interest_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('world_interest_events_id_seq', 1, false);


--
-- Name: world_interest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('world_interest_id_seq', 1, true);


--
-- Data for Name: world_worldborder; Type: TABLE DATA; Schema: public; Owner: geo
--

COPY world_worldborder (id, name, area, pop2005, fips, iso2, iso3, un, region, subregion, lon, lat, mpoly) FROM stdin;
\.


--
-- Name: world_worldborder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: geo
--

SELECT pg_catalog.setval('world_worldborder_id_seq', 1, false);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: users_confirmation_token_key; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_confirmation_token_key UNIQUE (confirmation_token);


--
-- Name: users_email_canonical_key; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_canonical_key UNIQUE (email_canonical);


--
-- Name: users_interests_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_interests
    ADD CONSTRAINT users_interests_pkey PRIMARY KEY (id);


--
-- Name: users_interests_user_id_interest_id_5a099a01_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_interests
    ADD CONSTRAINT users_interests_user_id_interest_id_5a099a01_uniq UNIQUE (user_id, interest_id);


--
-- Name: users_participatedin_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_participatedin
    ADD CONSTRAINT users_participatedin_pkey PRIMARY KEY (id);


--
-- Name: users_participatedin_user_id_event_id_b3ef071a_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_participatedin
    ADD CONSTRAINT users_participatedin_user_id_event_id_b3ef071a_uniq UNIQUE (user_id, event_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_username_canonical_key; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_canonical_key UNIQUE (username_canonical);


--
-- Name: world_availability_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_availability
    ADD CONSTRAINT world_availability_pkey PRIMARY KEY (id);


--
-- Name: world_event_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_event
    ADD CONSTRAINT world_event_pkey PRIMARY KEY (id);


--
-- Name: world_interest_events_interest_id_event_id_6c1e2f0d_uniq; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest_events
    ADD CONSTRAINT world_interest_events_interest_id_event_id_6c1e2f0d_uniq UNIQUE (interest_id, event_id);


--
-- Name: world_interest_events_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest_events
    ADD CONSTRAINT world_interest_events_pkey PRIMARY KEY (id);


--
-- Name: world_interest_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest
    ADD CONSTRAINT world_interest_pkey PRIMARY KEY (id);


--
-- Name: world_worldborder_pkey; Type: CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_worldborder
    ADD CONSTRAINT world_worldborder_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: users_confirmation_token_98a30b00_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_confirmation_token_98a30b00_like ON users USING btree (confirmation_token varchar_pattern_ops);


--
-- Name: users_email_canonical_8bf2bad6_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_email_canonical_8bf2bad6_like ON users USING btree (email_canonical varchar_pattern_ops);


--
-- Name: users_geog_id; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_geog_id ON users USING gist (geog);


--
-- Name: users_interests_interest_id_a061e82e; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_interests_interest_id_a061e82e ON users_interests USING btree (interest_id);


--
-- Name: users_interests_user_id_60f636b5; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_interests_user_id_60f636b5 ON users_interests USING btree (user_id);


--
-- Name: users_participatedin_event_id_50fa4a01; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_participatedin_event_id_50fa4a01 ON users_participatedin USING btree (event_id);


--
-- Name: users_participatedin_user_id_a0208ce8; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_participatedin_user_id_a0208ce8 ON users_participatedin USING btree (user_id);


--
-- Name: users_username_canonical_6e3e94c5_like; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX users_username_canonical_6e3e94c5_like ON users USING btree (username_canonical varchar_pattern_ops);


--
-- Name: world_availability_interest_id_8f59092d; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_availability_interest_id_8f59092d ON world_availability USING btree (interest_id);


--
-- Name: world_availability_user_id_e7def54f; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_availability_user_id_e7def54f ON world_availability USING btree (user_id);


--
-- Name: world_event_geog_id; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_event_geog_id ON world_event USING gist (geog);


--
-- Name: world_interest_events_event_id_8a753dc3; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_interest_events_event_id_8a753dc3 ON world_interest_events USING btree (event_id);


--
-- Name: world_interest_events_interest_id_e21488c9; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_interest_events_interest_id_e21488c9 ON world_interest_events USING btree (interest_id);


--
-- Name: world_worldborder_mpoly_id; Type: INDEX; Schema: public; Owner: geo
--

CREATE INDEX world_worldborder_mpoly_id ON world_worldborder USING gist (mpoly);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_interests_interest_id_a061e82e_fk_world_interest_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_interests
    ADD CONSTRAINT users_interests_interest_id_a061e82e_fk_world_interest_id FOREIGN KEY (interest_id) REFERENCES world_interest(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_interests_user_id_60f636b5_fk_users_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_interests
    ADD CONSTRAINT users_interests_user_id_60f636b5_fk_users_id FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_participatedin_event_id_50fa4a01_fk_world_event_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_participatedin
    ADD CONSTRAINT users_participatedin_event_id_50fa4a01_fk_world_event_id FOREIGN KEY (event_id) REFERENCES world_event(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_participatedin_user_id_a0208ce8_fk_users_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY users_participatedin
    ADD CONSTRAINT users_participatedin_user_id_a0208ce8_fk_users_id FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: world_availability_interest_id_8f59092d_fk_world_interest_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_availability
    ADD CONSTRAINT world_availability_interest_id_8f59092d_fk_world_interest_id FOREIGN KEY (interest_id) REFERENCES world_interest(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: world_availability_user_id_e7def54f_fk_users_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_availability
    ADD CONSTRAINT world_availability_user_id_e7def54f_fk_users_id FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: world_interest_events_event_id_8a753dc3_fk_world_event_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest_events
    ADD CONSTRAINT world_interest_events_event_id_8a753dc3_fk_world_event_id FOREIGN KEY (event_id) REFERENCES world_event(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: world_interest_events_interest_id_e21488c9_fk_world_interest_id; Type: FK CONSTRAINT; Schema: public; Owner: geo
--

ALTER TABLE ONLY world_interest_events
    ADD CONSTRAINT world_interest_events_interest_id_e21488c9_fk_world_interest_id FOREIGN KEY (interest_id) REFERENCES world_interest(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

